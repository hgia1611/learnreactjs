import React, { Component } from 'react';
import {
  withRouter
} from 'react-router-dom';
import { AppContext } from './Component/AppContext';
import Footer from './Component/Layout/Footer';
import Header from './Component/Layout/Header';
import MenuLeft from './Component/Layout/MenuLeft';
import Slider from './Component/Layout/Slider';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countProducts: 0
    }
    this.countProductCart = this.countProductCart.bind(this);
  }

  countProductCart(data) {
    this.setState({
      countProducts: data
    })
  }

  render() {
    let pathURL = this.props.location.pathname;
    return (
      <AppContext.Provider value={{
        state: this.state,
        countProductCart: this.countProductCart
      }}>
        <>
          <Header />
          {pathURL == '/' ? <Slider /> : ''}
          <section>
            <div className="container">
              <div className="row">
                {pathURL.includes('user') || pathURL.includes('login') || pathURL.includes('cart') ? '' : <MenuLeft />}
                {this.props.children}
              </div>
            </div>
          </section>
          <Footer />
        </>
      </AppContext.Provider>
    )
  }
}

export default withRouter(App);
