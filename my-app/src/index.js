import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router, Route, Switch
} from 'react-router-dom';
import SimpleReactLightbox from 'simple-react-lightbox';
import App from './App';
import BlogDetail from './Component/Blog/Detail';
import Blog from './Component/Blog/Index';
import Cart from './Component/Cart/Index';
import Home from './Component/Home/Index';
import LoginRegister from './Component/LoginRegister/Index';
import AddEditProduct from './Component/Product/AddEditProduct';
import DeleteProduct from './Component/Product/Delete';
import ProductDetail from './Component/Product/Detail';
import MyProduct from './Component/Product/MyProduct';
import UserUpdate from './Component/User/Update';
import './index.css';

ReactDOM.render(
  <div>
    <SimpleReactLightbox>
      <Router>
        <App>
          <Switch>
            <Route exact path="/" component={Home} />

            <Route exact path="/blog" component={Blog} />
            <Route path={"/blog/detail/:id"} component={BlogDetail} />

            <Route path="/login" component={LoginRegister} />
            <Route path={"/user/update/:id"} component={UserUpdate} />

            <Route path={"/user/my-product"} component={MyProduct} />
            <Route path={"/user/add-product"} component={AddEditProduct} />
            <Route path={"/user/edit-product/:id"} component={AddEditProduct} />
            <Route path={"/user/delete-product/:id"} component={DeleteProduct} />

            <Route path={"/product/detail/:id"} component={ProductDetail} />
            <Route path={"/product/cart"} component={Cart} />
          </Switch>
        </App>
      </Router>
    </SimpleReactLightbox>
  </div>,
  document.getElementById('root')
);
