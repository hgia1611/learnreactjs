import React, { Component } from 'react';

class FormErrors extends Component {
  constructor(props) {
    super(props);    
  }
  
  render() {
    let formErrors = this.props.formErrors;
    return (
      <div className='formErrors'>
        {Object.keys(formErrors).map((index, i) => {
          if(formErrors[index].length > 0){
            return (
              <p style={{color: "red"}} key={i}>{formErrors[index]}</p>
            )
          }else{
            return "";
          }
        })}
      </div>
    );
  }
}

export default FormErrors;