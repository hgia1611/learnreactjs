import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class MenuAccount extends Component {
  render() {
    let userData = JSON.parse(localStorage.getItem("userData"));
    let id;
    if (userData != null) {
      id = userData.auth.id
    }
    return (
      <div class="col-sm-3">
        <div class="left-sidebar">
          <h2>Account</h2>
          <div class="panel-group category-products" id="accordian">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"><Link to={"/user/update/" + id}>Account</Link></h4>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"><Link to="/user/my-product">My Product</Link></h4>
              </div>
            </div>
          </div>

          <div class="shipping text-center">
            <img src={window.location.origin + "/frontend/images/home/shipping.jpg"} alt="" />
          </div>
        </div>
      </div>
    );
  }
}

export default MenuAccount;