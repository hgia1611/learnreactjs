import axios from "axios";
import React, { Component } from "react";
import FormErrors from "../Errors/FormErrors";
import MenuAccount from './MenuAccount';

class Update extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      email: "",
      password: "",
      phone: "",
      address: "",
      country: "",
      avatar: "",
      file: "",
      formErrors: {},
      userData: JSON.parse(localStorage.getItem("userData"))
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleInputFile = this.handleInputFile.bind(this);
  }

  componentDidMount() {
    this.setState({
      id: this.state.userData.auth.id,
      name: this.state.userData.auth.name,
      email: this.state.userData.auth.email,
      phone: this.state.userData.auth.phone,
      address: this.state.userData.auth.address,
    });
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleUpdate(e) {
    e.preventDefault();
    let flag = true;
    let id = this.state.id;
    let errorSubmit = this.state.formErrors;

    if (!this.state.name) {
      flag = false;
      errorSubmit.name = "Vui long nhap name";
    } else {
      flag = true;
      errorSubmit.name = "";
    }
    if (!this.state.phone) {
      flag = false;
      errorSubmit.phone = "Vui long nhap phone";
    } else {
      flag = true;
      errorSubmit.phone = "";
    }
    if (!this.state.address) {
      flag = false;
      errorSubmit.address = "Vui long nhap address";
    } else {
      flag = true;
      errorSubmit.address = "";
    }
    // if (!this.state.avatar) {
    //   flag = false;
    //   errorSubmit.avatar = "Vui long nhap avatar";
    // } else {
    //   flag = true;
    //   errorSubmit.avatar = "";
    // }
    if (!flag) {
      this.setState({
        formErrors: errorSubmit,
      });
      return false;
    } else {
      let url = `http://localhost:8080/laravel/public/api/user/update/` + id;
      let accessToken = this.state.userData.auth_token;
      let config = {
        headers: {
          'Authorization': "Bearer " + accessToken,
          "Content-Type": "application/x-www-form-urlencoded",
          'Accept': "application/json",
        },
      };
      let formData = new FormData();
      formData.append("name", this.state.name);
      formData.append("email", this.state.userData.auth.email);
      formData.append("password", this.state.password);
      formData.append("phone", this.state.phone);
      formData.append("address", this.state.address);
      formData.append('country', this.state.country);
      formData.append("level", 0);
      formData.append("avatar", this.state.avatar);

      axios
        .post(url, formData, config)
        .then((res) => {
          if (res.data.errors) {
            console.log(res.data.errors);
            this.setState({
              formErrors: res.data.errors,
            });
          } else {
            // console.log(res.data.Auth.name)
            const userData = {
              auth_token: this.state.userData.auth_token,
              auth: res.data.Auth,
            }
            localStorage.setItem('userData', JSON.stringify(userData));
            alert("Update thanh cong!");
          }
        });
    }
  }

  handleInputFile(e) {
    const file = e.target.files;
    let reader = new FileReader();
    if (file[0]) {
      let typeImg = file[0].name.substr(file[0].name.lastIndexOf("."));
      let sizeImg = file[0].size;
      let validFileType = ".jpg, .jpeg, .png";

      if (validFileType.indexOf(typeImg) < 0) {
        alert("File ảnh sai định dạng!");
        e.target.value = null;
      }
      if (sizeImg > 1024 * 1024) {
        alert("Vui lòng chọn file ảnh dưới 1MB");
        e.target.value = null;
      } else {
        reader.onload = (e) => {
          this.setState({
            avatar: e.target.result,
            file: file[0],
          });
        };
      }
      reader.readAsDataURL(file[0]);
    }
  }

  render() {
    return (
      <>
        <MenuAccount />
        <div className="col-sm-4">
          <div className="signup-form">
            <h2>User Update!</h2>
            <form onSubmit={this.handleUpdate}>
              <input
                type="text"
                name="name"
                onChange={this.handleChange}
                defaultValue={this.state.name}
              />

              <input
                type="email"
                name="email"
                readOnly
                onChange={this.handleChange}
                defaultValue={this.state.email}
              />

              <input
                type="password"
                name="password"
                placeholder="Password"
                onChange={this.handleChange}
              />

              <input
                name="phone"
                onChange={this.handleChange}
                defaultValue={this.state.phone}
              />

              <input
                name="address"
                onChange={this.handleChange}
                defaultValue={this.state.address}
              />

              <input
                name="country"
                placeholder='Country'
                onChange={this.handleChange}
                defaultValue={this.state.country}
              />

              <input
                type="file"
                name="avatar"
                onChange={this.handleInputFile}
              />
              <button type="submit" className="btn btn-default">
                Update
              </button>
              <br />
              <FormErrors formErrors={this.state.formErrors} />
            </form>
          </div>
        </div>
      </>
    );
  }
}

export default Update;
