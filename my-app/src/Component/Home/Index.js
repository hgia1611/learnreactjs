import axios from "axios";
import React, { Component } from 'react';
import {
  Link
} from 'react-router-dom';
import BottomSlider from '../Layout/BottomSlider';
import { AppContext } from '../AppContext';

class Index extends Component {
  static contextType = AppContext;

  constructor(props) {
    super(props);
    this.state = {
      data: '',
      quantity: 1,
    }
    this.addCart = this.addCart.bind(this);
  }

  componentDidMount() {
    axios.get('http://localhost:8080/laravel/public/api/product').then((res) => {
      this.setState({
        data: res.data.data
      })
    })
  }

  addCart(e) {
    let cart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : {};
    let id = e.target.id.toString();

    cart[id] = (cart[id] ? cart[id] : 0);

    let qty = cart[id] + parseInt(this.state.quantity);

    cart[id] = qty; //Toán tử này sẽ gán biến id có giá trị là qty vào object cart
    localStorage.setItem('cart', JSON.stringify(cart))
    this.context.countProductCart(Object.values(cart).length);
  }

  renderProduct() {
    let { data } = this.state;
    if (data instanceof Array && data) {
      return data.map((item, i) => {
        let image = JSON.parse(item.image)
        if (i < 6) {
          return (
            <div key={i} className="col-sm-4">
              <div className="product-image-wrapper">
                <div className="single-products">
                  <div className="productinfo text-center">
                    <img src={"http://localhost:8080/laravel/public/upload/user/product/" + item.id_user + '/' + image[0]} alt="" />
                    <h2>${item.price}</h2>
                    <p>{item.name}</p>
                    <a id={item.id} className="btn btn-default add-to-cart" onClick={this.addCart}><i className="fa fa-shopping-cart" />Add to cart</a>
                  </div>
                  <div className="product-overlay">
                    <div className="overlay-content">
                      <Link to={'/product/detail/' + item.id}>
                        <h2>${item.price}</h2>
                        <p>{item.name}</p>
                      </Link>
                      <a id={item.id} className="btn btn-default add-to-cart" onClick={this.addCart}><i className="fa fa-shopping-cart" />Add to cart</a>
                    </div>
                  </div>
                </div>
                <div className="choose">
                  <ul className="nav nav-pills nav-justified">
                    <li><a href="#"><i className="fa fa-plus-square" />Add to wishlist</a></li>
                    <li><a href="#"><i className="fa fa-plus-square" />Add to compare</a></li>
                  </ul>
                </div>
              </div>
            </div>
          )
        }
      })
    }
  }

  render() {
    return (
      <>
        <div className="col-sm-9 padding-right">
          <div className="features_items">
            <h2 className="title text-center">Product</h2>
            {this.renderProduct()}
          </div>
          <BottomSlider />
        </div>
      </>
    );
  }
}

export default Index;