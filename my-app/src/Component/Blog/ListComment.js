import React, { Component } from "react";

class ListComment extends Component {
  constructor(props) {
    super(props);
    this.renderData = this.renderData.bind(this);
    this.idSubComment = this.idSubComment.bind(this);
  }

  idSubComment(e) {
    let id = e.target.id
    this.props.idSubComment(id);
  }

  renderData() {
    let commentData = {};
    commentData = this.props.commentData;
    if (commentData == undefined) {

    } else {
      if (Object.keys(commentData).length > 0) {
        return Object.keys(commentData).map((index, i) => {
          if (commentData[index].id_comment == 0) {
            return (
              <>
                <li className="media">
                  <a className="pull-left" href="#">
                    <img
                      className="media-object"
                      width="121px" height="86px"
                      src={
                        "http://localhost:8080/laravel/public/upload/user/avatar/" +
                        commentData[index].image_user
                      }
                      alt=""
                    />
                  </a>
                  <div className="media-body">
                    <ul className="sinlge-post-meta">
                      <li>
                        <i className="fa fa-user" />
                        {commentData[index].name_user}
                      </li>
                      <li>
                        <i className="fa fa-clock-o" /> 1:33 pm
                      </li>
                      <li>
                        <i className="fa fa-calendar" /> DEC 5, 2013
                      </li>
                    </ul>
                    <p>{commentData[index].comment}</p>
                    <a className="btn btn-primary" href="#repComment" id={commentData[index].id} onClick={this.idSubComment}>
                      <i className="fa fa-reply" />
                      Replay
                    </a>
                  </div>
                </li>
                {
                  Object.keys(commentData).map((index2, i) => {
                    if (commentData[index2].id_comment == commentData[index].id) {
                      return (
                        <li className="media second-media">
                          <a className="pull-left" href="#">
                            <img
                              className="media-object"
                              width="121px" height="86px"
                              src={
                                "http://localhost:8080/laravel/public/upload/user/avatar/" +
                                commentData[index2].image_user
                              }
                              alt=""
                            />
                          </a>
                          <div className="media-body">
                            <ul className="sinlge-post-meta">
                              <li>
                                <i className="fa fa-user" />
                                {commentData[index2].name_user}
                              </li>
                              <li>
                                <i className="fa fa-clock-o" /> 1:33 pm</li>
                              <li>
                                <i className="fa fa-calendar" /> DEC 5, 2013</li>
                            </ul>
                            <p>{commentData[index2].comment}</p>
                            <a className="btn btn-primary" href="#repComment" id={commentData[index2].id} onClick={this.idSubComment}>
                              <i className="fa fa-reply" />Replay</a>
                          </div>
                        </li>
                      );
                    }
                  })
                }
              </>
            );
          }
        });
      }
    }
  }

  render() {
    return (
      <div className="response-area">
        <h2>RESPONSES</h2>
        <ul className="media-list">
          {this.renderData()}
        </ul>
      </div>
    );
  }
}

export default ListComment;
