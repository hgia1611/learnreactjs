import axios from 'axios';
import React, { Component } from 'react';
import { Link } from "react-router-dom";

class Comment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: [],
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({
      comment: e.target.value
    })
  }

  handleSubmit(e) {
    const userData = JSON.parse(localStorage.getItem("userData"));
    if (!userData) {
      alert("Vui lòng đăng nhập!")
    } else {
      let url = 'http://localhost:8080/laravel/public/api/blog/comment/' + this.props.idBlog;
      let accessToken = userData.auth_token;
      let config = {
        headers: {
          "Authorization": "Bearer " + accessToken,
          "Content-Type": "application/x-www-form-urlencoded",
          "Accept": "application/json"
        }
      };
      let { comment } = this.state;
      if (comment) {
        const formData = new FormData();
        formData.append('id_blog', this.props.idBlog);
        formData.append('id_user', userData.auth.id);
        formData.append('id_comment', this.props.idSubComment ? this.props.idSubComment : 0);
        formData.append('comment', this.state.comment);
        formData.append('image_user', userData.auth.avatar);
        formData.append('name_user', userData.auth.name);
        axios.post(url, formData, config).then(res => {
          // console.log(res);
          if (res.data.data) {
            this.props.pushComment(res.data.data)
            this.setState({
              comment: ""
            });
          }
        })
      }
    }
  }

  render() {
    return (
      <div className="replay-box">
        <div className="row">
          <div className="col-sm-12">
            <h2>Leave a replay</h2>
            <div className="text-area">
              <div className="blank-arrow">
                <label>Your Name</label>
              </div>
              <span>*</span>
              <textarea id="repComment" name="message" onChange={this.handleChange} rows={11} defaultValue={""} value={this.state.comment} />
              <Link className="btn btn-primary" onClick={this.handleSubmit}>Comment</Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Comment;