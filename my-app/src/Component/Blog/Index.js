import axios from "axios";
import React, { Component } from "react";
import {
  Link
} from "react-router-dom";

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
    this.renderData = this.renderData.bind(this);
  }

  componentDidMount() {
    axios
      .get(`http://localhost:8080/laravel/public/api/blog`)
      .then((res) => {
        this.setState({
          data: res.data.blog.data,
        });
      })
      .catch((error) => console.log(error));
  }

  renderData() {
    let { data } = this.state;
    if (Object.keys(data).length > 0) {
      return Object.keys(data).map((index, i) => {
        return (
          <div className="single-blog-post">
            <Link to={"/blog/detail/" + data[index].id}>
              <h3>{data[index].title}</h3>
            </Link>
            <div class="post-meta">
              <ul>
                <li>
                  <i class="fa fa-user"></i> Mac Doe
                </li>
                <li>
                  <i class="fa fa-clock-o"></i> 1:33 pm
                </li>
                <li>
                  <i class="fa fa-calendar"></i> DEC 5, 2013
                </li>
              </ul>
              <span>
                {/* <Rate /> */}
              </span>
            </div>
            <a href="">
              <img
                src={
                  "http://localhost:8080/laravel/public/upload/blog/image/" +
                  data[index].image
                }
                alt=""
              />
            </a>
            <p>{data[index].description}</p>
            <a class="btn btn-primary" href="">
              Read More
            </a>
          </div>
        );
      });
    }
  }
  render() {
    return (
      <>
        <div className="col-sm-9">
          <div className="blog-post-area">
            <h2 className="title text-center">Latest From our Blog</h2>
            {this.renderData()}
          </div>
        </div>
      </>
    );
  }
}

export default Index;
