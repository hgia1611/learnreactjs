import Box from "@material-ui/core/Box";
// import StarRating from "react-star-ratings";
import Rating from "@material-ui/lab/Rating";
import axios from "axios";
import React, { Component } from "react";

class Rate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: 0,
      count: 0
    }
    this.pushRating = this.pushRating.bind(this);
  }

  componentDidMount() {
    let url = "http://localhost:8080/laravel/public/api/blog/rate/" + this.props.idBlog;
    axios.get(url).then((res) => {
      let data = res.data.data
      if (typeof data === 'object' && Object.keys(data).length > 0) {
        let sum = 0;
        Object.keys(data).map((index, i) => {
          sum = sum + data[index]['rate'];
        })
        let length = Object.keys(data).length;
        this.setState({
          rating: Math.round(sum / length),
          count: length
        })
      }
    }).catch((errors) => {
      console.log(errors)
    })
  }

  pushRating(e) {
    const userData = JSON.parse(localStorage.getItem("userData"));
    if (!userData) {
      alert("Vui lòng đăng nhập!");
    } else {
      alert("Bạn đã đánh giá blog " + e.target.defaultValue + " sao.");
      let url = "http://localhost:8080/laravel/public/api/blog/rate/" + this.props.idBlog;
      let accessToken = userData.auth_token;
      let config = {
        headers: {
          Authorization: "Bearer " + accessToken,
          "Content-Type": "application/x-www-form-urlencoded",
          Accept: "application/json",
        },
      };
      const formData = new FormData();
      formData.append('user_id', userData.auth.id);
      formData.append('blog_id', this.props.idBlog);
      formData.append('rate', e.target.defaultValue);
      axios.post(url, formData, config).then((res) => {

      })

      this.setState({
        rating: e.target.defaultValue
      })
    }
  }

  render() {
    return (
      <Box component="fieldset" mb={3}>
        <Rating
          value={this.state.rating}
          totalStars={5}
          starRatedColor="#FE980F"
          name="rating"
          size="large"
          onChange={this.pushRating}
        />
        {(this.state.count) + " votes"}
      </Box>
    );
  }
}

export default Rate;
