import axios from 'axios';
import React, { Component } from 'react';
import {
  Link
} from 'react-router-dom';
import DeleteProduct from '../Product/Delete';
import MenuAccount from '../User/MenuAccount';
import './MyProduct.css';

class MyProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      userData: JSON.parse(localStorage.getItem('userData'))
    }
    this.removeProduct = this.removeProduct.bind(this);
  }

  componentDidMount() {
    let url = 'http://localhost:8080/laravel/public/api/user/my-product';
    let accessToken = this.state.userData.auth_token;
    let config = {
      headers: {
        'Authorization': "Bearer " + accessToken,
        'Content-Type': "application/x-www-form-urlencoded",
        'Accept': "application/json",
      },
    };
    axios.get(url, config).then((res) => {
      this.setState({
        data: res.data.data
      })
    });
  }

  removeProduct(data) {
    this.setState({
      data: data
    })
  }

  renderData() {
    let { data } = this.state;
    if (Object.keys(data).length > 0) {
      return Object.keys(data).map((index, i) => {
        let image = JSON.parse(data[index].image)
        return (
          <tr key={i}>
            <td>{data[index].id}</td>
            <td>{data[index].name}</td>
            <td><img width="70px" height="70px" src={"http://localhost:8080/laravel/public/upload/user/product/" + data[index].id_user + '/' + image[0]} alt="" /></td>
            <td>${data[index].price}</td>
            <td>
              <Link to={'/user/edit-product/' + data[index].id}>
                Sửa
              </Link> <br /><br />
              <DeleteProduct idProduct={data[index].id} accessToken={this.state.userData.auth_token} removeProduct={this.removeProduct} />
            </td>
          </tr>
        )
      })
    }
  }

  render() {
    return (
      <>
        <MenuAccount />
        <div className="col-sm-4">
          <section id="cart_items">
            <div>
              <div className="breadcrumbs">
                <ol className="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li className="active">My Product</li>
                </ol>
              </div>
              <div>
                <table className="myproduct_table">
                  <tr>
                    <th className="id">ID</th>
                    <th className="name">Name</th>
                    <th className="image">Image</th>
                    <th className="price">Price</th>
                    <th className="action">Action</th>
                  </tr>
                  <tbody>
                    {this.renderData()}
                  </tbody>
                </table>
              </div>
              <Link to='/user/add-product' className="btn btn-default get">Thêm mới</Link>
            </div>
          </section>
        </div>
      </>
    );
  }
}

export default MyProduct;