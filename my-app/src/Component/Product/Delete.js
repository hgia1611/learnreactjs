import axios from 'axios';
import React, { Component } from 'react';

class Delete extends Component {
  constructor(props) {
    super(props);
    this.deleteProduct = this.deleteProduct.bind(this);
  }

  deleteProduct() {
    let url = 'http://localhost:8080/laravel/public/api/user/delete-product/' + this.props.idProduct;
    let config = {
      headers: {
        'Authorization': "Bearer " + this.props.accessToken,
        "Content-Type": "application/x-www-form-urlencoded",
        'Accept': "application/json",
      }
    }
    axios.get(url, config).then(res => {
      this.props.removeProduct(res.data.data)
    })
  }

  render() {
    return (
      <a style={{ cursor: "pointer" }} onClick={this.deleteProduct}>Xoá</a>
    );
  }
}

export default Delete;