import axios from 'axios';
import React, { Component } from 'react';
import FormErrors from '../Errors/FormErrors';
import MenuAccount from '../User/MenuAccount';

const imgStyle = {
  width: '50px',
  height: '50px'
};

class AddEditProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listCategory: '',
      listBrand: '',
      category: '',
      brand: '',
      name: '',
      price: '',
      status: '',
      sale: 0,
      avatar: '',
      company: '',
      detail: '',
      listImg: [],
      formErrors: {},
      userData: JSON.parse(localStorage.getItem('userData')),

      //Edit
      avatarOld: '',
      avatarChecked: [],

    }
    this.handleChange = this.handleChange.bind(this);
    this.handleInputFile = this.handleInputFile.bind(this);
    this.renderImage = this.renderImage.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    axios.get(`http://localhost:8080/laravel/public/api/category-brand`).then((res) => {
      this.setState({
        listCategory: res.data.category,
        listBrand: res.data.brand
      })
    });

    let idProduct = this.props.match.params.id;
    if (idProduct) {
      let url = `http://localhost:8080/laravel/public/api/user/product/` + idProduct;
      let accessToken = this.state.userData.auth_token;
      let config = {
        headers: {
          'Authorization': 'Bearer ' + accessToken,
          'Content-Type': 'multipart/form-data',
          'Accept': 'application/json'
        }
      }
      axios.get(url, config).then(res => {
        this.setState({
          name: res.data.data.name,
          price: res.data.data.price,
          category: res.data.data.id_category,
          brand: res.data.data.id_brand,
          company: res.data.data.company_profile,
          detail: res.data.data.detail,
          status: res.data.data.status,
          sale: res.data.data.sale,
          avatarOld: res.data.data.image
        })
      }).catch(errors => {

      })
    }
  }

  handleChange(e) {
    let avatarChecked = this.state.avatarChecked;
    if (e.target.name === 'avatarCheck') {
      if (e.target.checked) {
        avatarChecked.push(e.target.value)
      } else {
        let item = avatarChecked.indexOf(e.target.value);
        if (item > -1) {
          avatarChecked.splice(item, 1)
        }
      }
    }
    // console.log(avatarChecked)
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  renderCategory() {
    if (Array.isArray(this.state.listCategory) && this.state.listCategory.length > 0) {
      return this.state.listCategory.map((index, i) => {
        return (
          <option key={i} value={index.id}>{index.category}</option>
        )
      })
    }
  }

  renderBrand() {
    if (Array.isArray(this.state.listBrand) && this.state.listBrand.length > 0) {
      return this.state.listBrand.map((index, i) => {
        return (
          <option key={i} value={index.id}>{index.brand}</option>
        )
      })
    }
  }

  handleInputFile(e) {
    const fileInput = e.target.files;
    const fileLength = fileInput.length;
    const { avatarChecked, avatarOld } = this.state;
    if (fileInput[0] && fileLength <= 3) {
      let typeImg = fileInput[0].name.substr(fileInput[0].name.lastIndexOf("."));
      let sizeImg = fileInput[0].size;
      let validFileType = ".jpg, .jpeg, .png";

      if (validFileType.indexOf(typeImg) < 0) {
        alert("File ảnh sai định dạng!");
        e.target.value = null;
      }
      if (sizeImg > 1024 * 1024) {
        alert("Vui lòng chọn file ảnh dưới 1MB");
        e.target.value = null;
      } else {
        this.setState({
          avatar: fileInput
        })
      }
    }
    if ((avatarOld.length - avatarChecked.length + fileLength) > 3) {
      e.target.value = null;
      alert("Chỉ được thêm 3 ảnh");
    }
  }

  renderImage() {
    const { avatarOld } = this.state;
    let userID = this.state.userData.auth.id;
    if (Array.isArray(avatarOld) && avatarOld.length > 0) {
      return avatarOld.map((item, i) => {
        return (
          <li style={{ display: 'inline-block', margin: '0 10px' }} key={i}>
            <img width="100" height="100" src={`http://localhost:8080/laravel/public/upload/user/product/` + userID + `/` + item} />
            <input name='avatarCheck' type='checkbox' value={item} onChange={this.handleChange} />
          </li>
        )
      })
    }
  }

  handleSubmit(e) {
    e.preventDefault();

    let flag = true;
    let name = this.state.name;
    let price = this.state.price;
    let category = this.state.category;
    let brand = this.state.brand;
    let status = this.state.status;
    let sale = this.state.sale;
    let avatar = this.state.avatar;
    let company = this.state.company;
    let detail = this.state.detail;
    let formErrors = this.state.formErrors;
    let userID = this.state.userData.auth.id;

    formErrors.name = formErrors.price = formErrors.category = formErrors.brand = formErrors.status = formErrors.sale = formErrors.avatar = '';

    if (!name) {
      flag = false;
      formErrors.name = 'Vui long nhap Name';
    }
    if (!price) {
      flag = false;
      formErrors.price = 'Vui long nhap Price';
    } else if (isNaN(price)) {
      flag = false;
      formErrors.price = 'Price phai la number';
    }
    if (!category) {
      flag = false;
      formErrors.category = 'Vui long nhap Category';
    }
    if (!brand) {
      flag = false;
      formErrors.brand = 'Vui long nhap Brand';
    }
    if (!status) {
      flag = false;
      formErrors.status = 'Vui long nhap Status';
    }
    if (status == 2 && !sale) {
      flag = false;
      formErrors.sale = 'Vui long nhap gia Sale';
    }
    if (!avatar && !Number(this.props.match.params.id)) {
      flag = false;
      formErrors.avatar = 'Vui long nhap Avatar';
    }
    if (!company) {
      flag = false;
      formErrors.company = 'Vui long nhap Company';
    }
    if (!detail) {
      flag = false;
      formErrors.detail = 'Vui long nhap Detail';
    }
    if (!flag) {
      this.setState({
        formErrors: formErrors
      })
    } else {
      let formData = new FormData();
      formData.append('name', this.state.name);
      formData.append('price', this.state.price);
      formData.append('category', this.state.category);
      formData.append('brand', this.state.brand);
      formData.append('company', this.state.company);
      formData.append('detail', this.state.detail);
      formData.append('status', this.state.status);
      formData.append('sale', this.state.sale);

      let accessToken = this.state.userData.auth_token;
      let config = {
        headers: {
          'Authorization': 'Bearer ' + accessToken,
          'Content-Type': 'multipart/form-data',
          'Accept': 'application/json'
        }
      };

      // console.log(avatar)
      Object.keys(avatar).map((index, i) => {
        // console.log(avatar[index])
        formData.append("file[]", avatar[index]);
      });

      if (!Number(this.props.match.params.id)) {
        let url = `http://localhost:8080/laravel/public/api/user/add-product`;
        axios.post(url, formData, config).then(res => {
          // console.log(res);
          if (res.data.response == 'success') {
            this.props.history.push('/user/my-product');
          } else {
            this.setState({
              formErrors: res.data.errors
            })
          }
        }).catch(errors => {

        });
      } else {
        let url = `http://localhost:8080/laravel/public/api/user/edit-product/` + this.props.match.params.id;

        Object.keys(this.state.avatarChecked).map((item, i) => {
          formData.append('avatarCheckBox[]', this.state.avatarChecked[item])
        });
        axios.post(url, formData, config).then(res => {
          if (res.data.response == 'success') {
            this.props.history.push("/user/my-product");
          }
          else {
            this.setState({
              formErrors: res.data.errors
            })
          }
        }).catch(errors => {
          console.log(errors)
        })
      }
    }
  }

  render() {
    return (
      <>
        <MenuAccount />
        <div className="col-sm-4">
          <div className="signup-form">
            {
              window.location.pathname.indexOf('add-product') > 0 ?
                <>
                  <h2>Create new product!</h2>
                </> :
                <>
                  <h2>Edit product!</h2>
                </>
            }
            <form onSubmit={this.handleSubmit}>
              <input
                type="text"
                name="name"
                placeholder="Name"
                value={this.state.name}
                onChange={this.handleChange}
              />
              <input
                type="text"
                name="price"
                placeholder="Price"
                value={this.state.price}
                onChange={this.handleChange}
              />
              <select value={this.state.category} name="category" onChange={this.handleChange}>
                <option value="">Please choose Category</option>
                {this.renderCategory()}
              </select>
              <select value={this.state.brand} name="brand" onChange={this.handleChange}>
                <option value="">Please choose Brand</option>
                {this.renderBrand()}
              </select>
              <input
                type="text"
                name="company"
                placeholder="Company"
                value={this.state.company}
                onChange={this.handleChange}
              />
              <textarea name='detail' placeholder='Detail' value={this.state.detail} onChange={this.handleChange}></textarea>
              <select name="status" value={this.state.status} onChange={this.handleChange}>
                <option value="">Please choose Status</option>
                <option value='1'>New</option>
                <option value='2'>Sale</option>
              </select>
              {this.state.status == "2" ?
                <>
                  <input
                    type="text"
                    name="sale"
                    placeholder="Sale"
                    value={this.state.sale}
                    onChange={this.handleChange}
                  />
                </> :
                <>
                </>}
              <input
                type="file"
                multiple
                name="avatar"
                onChange={this.handleInputFile}
              />
              <div className='render-image'>
                <ul>
                  {this.renderImage()}
                </ul>
              </div>
              {
                window.location.pathname.indexOf('add-product') > 0 ?
                  <>
                    <button type="submit" className="btn btn-default">
                      Create
                    </button>
                  </> :
                  <>
                    <button type="submit" className="btn btn-default">
                      Edit
                    </button>
                  </>
              }
              <FormErrors formErrors={this.state.formErrors} />
            </form>
          </div>
        </div>
      </>
    );
  }
}

export default AddEditProduct;