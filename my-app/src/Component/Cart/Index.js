import axios from 'axios';
import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import CartItems from './CartItems';
import { AppContext } from '../AppContext';

class Index extends Component {
  static contextType = AppContext;

  constructor(props) {
    super(props);
    this.state = {
      cartItems: '',
      total: '',
      qty: '',
    }
  }

  componentDidMount() {
    let cart = localStorage.getItem('cart');
    if (cart) {
      axios.post(`http://localhost:8080/laravel/public/api/product/cart`, cart).then((res) => {
        if (res.data.response == 'success') {
          let total = 0;
          let cartItems = res.data.data;
          cartItems.map((item, i) => {
            total += item.price * item.qty;
          });
          this.setState({
            cartItems: cartItems,
            total: total,
            qty: ''
          })
        } else {

        }
      }).catch((errors) => {
        console.log(errors)
      })
    }
  }

  renderTotal = () => {
    let cart = JSON.parse(localStorage.getItem('cart'));
    let cartItems = this.state.cartItems;
    let qty, total = 0;
    for (let i = 0; i < cartItems.length; i++) {
      qty = cart[cartItems[i].id];
      total += qty * cartItems[i].price;
    }
    this.setState({
      total: total
    })
  }

  deleteProduct = (id) => {
    const cart = JSON.parse(localStorage.getItem('cart'));
    delete cart[id];
    localStorage.setItem('cart', JSON.stringify(cart));

    let cart2 = localStorage.getItem('cart');
    if (cart2) {
      axios.post(`http://localhost:8080/laravel/public/api/product/cart`, cart2).then((res) => {
        if (res.data.response == 'success') {
          let total = 0;
          let cartItems = res.data.data;
          cartItems.map((item, i) => {
            total += item.price * item.qty;
          });
          this.setState({
            cartItems: cartItems,
            total: total,
            qty: ''
          });
          this.context.countProductCart(cartItems.length);
        } else {

        }
      }).catch((errors) => {
        console.log(errors)
      })
    }
  }

  renderData() {
    let cartItems = this.state.cartItems;
    if (cartItems) {
      return (
        <section id="cart_items">
          <div className="container">
            <div className="breadcrumbs">
              <ol className="breadcrumb">
                <li><a href="#">Home</a></li>
                <li className="active">Shopping Cart</li>
              </ol>
            </div>
            <div className="table-responsive cart_info">
              <table className="table table-condensed">
                <thead>
                  <tr className="cart_menu">
                    <td className="image">Name</td>
                    <td className="price">Price</td>
                    <td className="quantity">Quantity</td>
                    <td className="total">Total</td>
                    <td />
                  </tr>
                </thead>
                <tbody className="show_carts">
                  {
                    cartItems.map((cartitems) => {
                      return (
                        <CartItems key={cartitems.id} cartitems={cartitems} renderTotal={this.renderTotal} deleteProduct={this.deleteProduct} />
                      )
                    })
                  }
                </tbody>
              </table>
            </div>
            <span style={{ marginBottom: '40px', fontSize: '35px', color: 'red', fontWeight: 600 }}>
              Totals: <NumberFormat value={this.state.total} displayType={'text'} thousandSeparator={true} prefix={'$'} />
            </span>
          </div>
        </section >
      )
    } else {
      return (
        <h1 style={{ textAlign: 'center', margin: '150px' }}>
          Giỏ hàng trống
        </h1>
      )
    }
  }

  render() {
    return (
      <>
        {this.renderData()}
      </>
    );
  }
}

export default Index;