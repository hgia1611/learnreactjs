import React, { Component } from 'react';
import NumberFormat from 'react-number-format';

class CartItems extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qty: '',
      price: '',
      total: '',
      allTotals: ''
    }
    this.reductionProduct = this.reductionProduct.bind(this);
    this.increaseProduct = this.increaseProduct.bind(this);
    this.handleProductID = this.handleProductID.bind(this);
  }

  componentDidMount() {
    const { cartitems } = this.props;
    this.setState({
      qty: cartitems.qty,
      price: cartitems.price,
      total: cartitems.price * cartitems.qty
    })
  }

  reductionProduct(e) {
    const { renderTotal, deleteProduct } = this.props
    let cart = JSON.parse(localStorage.getItem('cart'));
    let id = e.target.id;
    let qty = cart[id];

    if (qty > 0) {
      qty = qty - 1;
      cart[id] = qty;
      localStorage.setItem('cart', JSON.stringify(cart));
      this.setState({
        qty: qty,
        total: this.state.price * qty
      })
      renderTotal();
    }
    if (qty <= 0) {
      deleteProduct(id);
    }
  }

  increaseProduct(e) {
    const { renderTotal } = this.props;
    let cart = JSON.parse(localStorage.getItem('cart'));
    let id = e.target.id;
    let qty = cart[id];

    qty = qty + 1;
    cart[id] = qty;
    localStorage.setItem('cart', JSON.stringify(cart));
    this.setState({
      qty: qty,
      total: this.state.price * qty
    })
    renderTotal();
  }

  handleProductID(e) {
    const { deleteProduct } = this.props;
    deleteProduct(e.target.id);
  }

  render() {
    const { cartitems } = this.props;
    return (
      <tr>
        <td>{cartitems.name}</td>
        <td>
          <NumberFormat value={cartitems.price} displayType={'text'} thousandSeparator={true} prefix={'$'} />
        </td>
        <td>
          <a id={cartitems.id} onClick={this.reductionProduct} style={{ margin: '5px', cursor: 'pointer', fontSize: '22px' }}>-</a>
          <input type="text" value={this.state.qty} name="qty" size="2" disabled />
          <a id={cartitems.id} onClick={this.increaseProduct} style={{ margin: '5px', cursor: 'pointer', fontSize: '22px' }}>+</a>
        </td>
        <td>
          <NumberFormat value={this.state.total} displayType={'text'} thousandSeparator={true} prefix={'$'} />
        </td>
        <td id={cartitems.id} onClick={this.handleProductID} style={{ cursor: 'pointer' }}>x</td>
      </tr>
    );
  }
}

export default CartItems;