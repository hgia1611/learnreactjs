import axios from "axios";
import React, { Component } from "react";
import FormErrors from "../Errors/FormErrors";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      password: "",
      phone: "",
      address: "",
      avatar: "",
      file: "",
      level: 0,
      formErrors: {},
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputFile = this.handleInputFile.bind(this);
  }

  handleChange(e) {
    const nameInput = e.target.name;
    const value = e.target.value;

    this.setState({
      [nameInput]: value,
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    let flag = true;
    let name = this.state.name;
    let email = this.state.email;
    let password = this.state.password;
    let phone = this.state.phone;
    let address = this.state.address;
    let avatar = this.state.avatar;
    let level = this.state.level;
    let errorSubmit = this.state.formErrors;

    if (!name) {
      flag = false;
      errorSubmit.name = "Vui long nhap name";
    } else {
      flag = true;
      errorSubmit.name = "";
    }
    if (!email) {
      flag = false;
      errorSubmit.email = "Vui long nhap email";
    } else {
      flag = true;
      errorSubmit.email = "";
    }
    if (!password) {
      flag = false;
      errorSubmit.password = "Vui long nhap password";
    } else {
      flag = true;
      errorSubmit.password = "";
    }
    if (!phone) {
      flag = false;
      errorSubmit.phone = "Vui long nhap phone";
    } else {
      flag = true;
      errorSubmit.phone = "";
    }
    if (!address) {
      flag = false;
      errorSubmit.address = "Vui long nhap address";
    } else {
      flag = true;
      errorSubmit.address = "";
    }
    if (!avatar) {
      flag = false;
      errorSubmit.avatar = "Vui long nhap avatar";
    } else {
      flag = true;
      errorSubmit.avatar = "";
    }

    if (!flag) {
      this.setState({
        formErrors: errorSubmit,
      });
      return false;
    }

    const user = {
      name,
      email,
      password,
      phone,
      address,
      avatar,
      level,
    };
    // console.log(user);

    axios
      .post(`http://localhost:8080/laravel/public/api/register`, user)
      .then((res) => {
        if (res.data.errors) {
          console.log(res.data.errors);
          this.setState({
            formErrors: res.data.errors,
          });
        } else {
        }
      });
  }

  handleInputFile(e) {
    const file = e.target.files;
    let reader = new FileReader();

    let typeImg = file[0].name.substr(file[0].name.lastIndexOf("."));
    let sizeImg = file[0].size;
    let validFileType = ".jpg, .jpeg, .png";

    if (validFileType.indexOf(typeImg) < 0) {
      alert("File ảnh sai định dạng!");
      e.target.value = null;
      return false;
    }
    if (sizeImg > 1024 * 1024) {
      alert("Vui lòng chọn file ảnh dưới 1MB");
      e.target.value = null;
      return false;
    } else {
      reader.onload = (e) => {
        this.setState({
          avatar: e.target.result,
          file: file[0],
        });
      };
    }
    reader.readAsDataURL(file[0]);
  }

  render() {
    return (
      <>
        <h2>New User Signup!</h2>
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            name="name"
            placeholder="Name"
            onChange={this.handleChange}
          />

          <input
            type="email"
            name="email"
            placeholder="Email Address"
            onChange={this.handleChange}
          />

          <input
            type="password"
            name="password"
            placeholder="Password"
            onChange={this.handleChange}
          />

          <input
            name="phone"
            placeholder="Phone"
            onChange={this.handleChange}
          />

          <input
            name="address"
            placeholder="Address"
            onChange={this.handleChange}
          />

          <input type="file" name="avatar" onChange={this.handleInputFile} />
          <button type="submit" className="btn btn-default">
            Signup
          </button>
          <br />
          <FormErrors formErrors={this.state.formErrors} />
        </form>
      </>
    );
  }
}

export default Register;
