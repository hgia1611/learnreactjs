import axios from "axios";
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import FormErrors from "../Errors/FormErrors";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      level: 0,
      formErrors: {},
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    let flag = true;
    const { email, password } = this.state;
    let errorSubmit = this.state.formErrors;

    if (!email) {
      flag = false;
      errorSubmit.email = "Vui long nhap email";
    } else {
      flag = true;
      errorSubmit.email = "";
    }
    if (!password) {
      flag = false;
      errorSubmit.password = "Vui long nhap password";
    } else {
      flag = true;
      errorSubmit.password = "";
    }
    if (!flag) {
      this.setState({
        formErrors: errorSubmit,
      });
    } else {
      let data = {
        email: email,
        password: password,
        level: 0,
      };

      axios
        .post("http://localhost:8080/laravel/public/api/login", data)
        .then((res) => {
          if (res.data.errors) {
            this.setState({
              formErrors: res.data.errors,
            });
          } else {
            let userData = {
              auth_token: res.data.success.token,
              auth: res.data.Auth,
            };
            localStorage.setItem("userData", JSON.stringify(userData));
            this.props.history.push("/");
          }
        });
    }
  }

  render() {
    return (
      <>
        <h2>Login to your account</h2>
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            name="email"
            placeholder="Email Address"
            onChange={this.handleChange}
          />

          <input
            type="password"
            name="password"
            placeholder="Password"
            onChange={this.handleChange}
          />
          <br />
          <span>
            <input type="checkbox" className="checkbox" />
            Keep me signed in
          </span>
          <button type="submit" className="btn btn-default">
            Login
          </button>
          <br />
          <FormErrors formErrors={this.state.formErrors} />
        </form>
      </>
    );
  }
}

export default withRouter(Login);
