import React, { Component } from "react";
import Login from "../LoginRegister/Login";
import Register from "../LoginRegister/Register";

class Index extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    // const [token, setToken] = this.state;

    // if(!token){
    //     return <Login setToken={setToken}/>
    // }

    return (
      <>
        <section id="form">
          <div className="container">
            <div className="row">
              <div className="col-sm-4 col-sm-offset-1">
                <div className="login-form">
                  <Login />
                </div>
              </div>
              <div className="col-sm-1">
                <h2 className="or">OR</h2>
              </div>
              <div className="col-sm-4">
                <div className="signup-form">
                  <Register />
                </div>
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
}

export default Index;
