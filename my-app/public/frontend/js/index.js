$(document).ready(function(){
    var object = {};
    $(".add-to-cart").click(function(){
        var getId, getImg, getName, getPrice, getQty;

        getId = $(this).closest('.single-products').find(".productinfo a").attr("id");
        getImg = $(this).closest('.single-products').find(".productinfo img").attr("src");
        getName = $(this).closest('.single-products').find(".productinfo p").text();
        getPrice = parseInt($(this).closest('.single-products').find(".productinfo h2").text().substr(1));
        getQty = 1;

        var objCon = {
            ID: getId,
            Img: getImg,
            Name: getName,
            Price: getPrice,
            Qty: getQty
        };

        //add moi
        var getLocal = localStorage.getItem("index");
        if(getLocal){
            object = JSON.parse(getLocal);
            Object.keys(object).map(function(key, index){
                if(object[key]["ID"] == getId){
                    object[key]["Qty"] += 1;
                    objCon = object[key];
                }
            })
        }

        object[getId] = objCon;
        var convert = JSON.stringify(object);
        localStorage.setItem("index", convert);

        // console.log(object);
    });
});

