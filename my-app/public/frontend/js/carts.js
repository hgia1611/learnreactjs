$(document).ready(function(){
    var getLocal = localStorage.getItem("index");
    var getId, getImg, getName, getPrice, getQty;

    if(getLocal){
        var object = JSON.parse(getLocal); //chuyen tu json sang object
        var html = "";
        Object.keys(object).map(function(key, index){
            getId = object[key]["ID"];
            getImg = object[key]["Img"];
            getName = object[key]["Name"];
            getPrice = object[key]["Price"];
            getQty = object[key]["Qty"];
            getTotal = getQty * getPrice;

          
            html += '<tr id="' + getId + '">' + 
                        "<td class='cart_product'>" +
                            "<a><img src='" + getImg + "' width='110px' height='110px'></a>" +
                        "</td>"+
                        '<td class="cart_description">'+
                            '<h4><a href="">' + getName + '</a></h4>'+
                            '<p>Web ID: ' + getId + '</p>'+
                        '</td>'+
                        '<td class="cart_price">'+
                            '<p>$'+ getPrice +'</p>'+
                        '</td>'+
                        '<td class="cart_quantity">'+
                            '<div class="cart_quantity_button">'+
                                '<a class="cart_quantity_down"> - </a>'+
                                '<input class="cart_quantity_input" type="text" name="quantity" value="'+ getQty + '" autocomplete="off" size="2">'+                        
                               '<a class="cart_quantity_up"> + </a>'+
                               '</div>'+
                        '</td>'+
                        '<td class="cart_total">'+
                            '<p class="cart_total_price">$' + getTotal + '</p>'+
                        '</td>'+
                        '<td class="cart_delete">'+
                            '<a class="cart_quantity_delete"><i class="fa fa-times"></i></a>'+
                        '</td>'+
                    '</tr>';
        });
        // var totalPrice = getTotal+=getTotal;
        // $(".total_price").text("$" + totalPrice);
        // console.log(getTotal+=getTotal)
        $(".show_carts").append(html);

    
        // add quantity
        $(".cart_quantity_up").click(function(){
            var getIdC = $(this).closest("tr").attr("id");
            var getQtyVal = parseInt($(this).closest(".cart_quantity_button").find("input").val());            
            $(this).closest(".cart_quantity_button").find("input").val(getQtyVal + 1);

            var getPriceVal = parseInt($(this).closest("tr").find(".cart_price p").text().substr(1));
            var tong  = (getQtyVal + 1) * getPriceVal;
            $(this).closest("tr").find(".cart_total_price").text('$' + tong);

            Object.keys(object).map(function(key, index){
                if(object[key]["ID"] == getIdC){
                    object[key]["Qty"] = getQtyVal + 1;
                    objCon = object[key];
                    var convert = JSON.stringify(object);
                    localStorage.setItem("index", convert);
                }
            });
            var tong = 0;
            Object.keys(object).map(function(key, index){
                tong += object[key]["Price"] * object[key]["Qty"];
            });
            $(".total_price").text("$" + tong);
        });

        // remove quantity
        $(".cart_quantity_down").click(function(){
            var getIdC = $(this).closest("tr").attr("id");
            var getQtyVal = parseInt($(this).closest(".cart_quantity_button").find("input").val());            
            $(this).closest(".cart_quantity_button").find("input").val(getQtyVal - 1);

            var getPriceVal = parseInt($(this).closest("tr").find(".cart_price p").text().substr(1));
            var tong  = (getQtyVal - 1) * getPriceVal;
            $(this).closest("tr").find(".cart_total_price").text('$' + tong);
            var getHtml = $(this).closest("tr");

            Object.keys(object).map(function(key, index){
                if(object[key]["ID"] == getIdC){
                    if(object[key]["Qty"] <= 1){
                        getHtml.remove();
                        delete object[key];
                        objCon[getIdC] = object[key];                       
                    }
                    else{
                        object[key]["Qty"] = getQtyVal - 1;
                        objCon[getIdC] = object[key];                       
                    }
                }
                var convert = JSON.stringify(object);
                localStorage.setItem("index", convert);
            });
                
            var tong = 0;
            Object.keys(object).map(function(key, index){
                tong += object[key]["Price"] * object[key]["Qty"];
            });
            $(".total_price").text("$" + tong);
        });

        // delete product
        $(".cart_quantity_delete").click(function(){
            var getIdC = $(this).closest("tr").attr("id");
            var getHtml = $(this).closest("tr");

            Object.keys(object).map(function(key, index){            
                if(object[key]["ID"] == getIdC){
                    delete object[key]; 
                    var convert = JSON.stringify(object);
                    localStorage.setItem("index", convert);
                    getHtml.remove();
                }
            });
            var tong = 0;
            Object.keys(object).map(function(key, index){

                tong += object[key]["Price"] * object[key]["Qty"];
            });
            $(".total_price").text("$" + tong);
        });

        var tong = 0;
        Object.keys(object).map(function(key, index){
            tong += object[key]["Price"] * object[key]["Qty"];
        });
        $(".total_price").text("$" + tong);
    }
});